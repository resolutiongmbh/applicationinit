package de.resolution.applicationinit;

import com.atlassian.crowd.embedded.api.CrowdDirectoryService;
import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.license.JiraLicenseManager;
import com.atlassian.plugin.spring.scanner.annotation.component.JiraComponent;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.JiraImport;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Nonnull;
import java.util.*;

@JiraComponent
@ExportAsService
public class JiraApplicationInit extends ApplicationInit {

    private static final Logger logger = LoggerFactory.getLogger(JiraApplicationInit.class);

    private final ApplicationProperties applicationProperties;
    private final JiraLicenseManager jiraLicenseManager;

    @Autowired
    public JiraApplicationInit(
            @JiraImport PluginSettingsFactory pluginSettingsFactory,
            @JiraImport CrowdService crowdService,
            @JiraImport CrowdDirectoryService crowdDirectoryService,
            @JiraImport ApplicationProperties applicationProperties,
            @JiraImport JiraLicenseManager jiraLicenseManager) {
        super(pluginSettingsFactory,crowdService,crowdDirectoryService);
        this.applicationProperties = applicationProperties;
        this.jiraLicenseManager = jiraLicenseManager;
        run();
    }

    @Override
    public void setLicense(@Nonnull String licenseString) {
        try {
            List<String> installedLicenseStrings = new ArrayList<>();
            jiraLicenseManager.getLicenses().forEach(lic -> installedLicenseStrings.add(lic.getLicenseString()));

            List<String> newLicenseStrings = Arrays.asList(licenseString.split(","));
            Collections.sort(newLicenseStrings);
            Collections.sort(installedLicenseStrings);

            if(newLicenseStrings.equals(installedLicenseStrings)) {
                logger.warn("*** licenses are up to date, no need to change.");
            } else {
                boolean first = true;
                for(String licString: newLicenseStrings) {
                    if(first) {
                        jiraLicenseManager.clearAndSetLicense(licString);
                        first = false;
                    } else {
                        jiraLicenseManager.setLicense(licString);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Setting license failed",e);
        }
    }

    @Override
    public void setBaseUrl(@Nonnull String baseUrl) {
        String currentBaseUrl = applicationProperties.getString(APKeys.JIRA_BASEURL);
        if(Objects.equals(baseUrl,currentBaseUrl)) {
            logger.warn("*** Base url {} is already set, no need to update", currentBaseUrl);
        } else {
            applicationProperties.setString(APKeys.JIRA_BASEURL, baseUrl);
            logger.warn("*** Updated base url from {} to {}", currentBaseUrl, baseUrl);
        }
    }

    @Override
    public void setTitle(@Nonnull String title) {
        String currentTitle = applicationProperties.getString(APKeys.JIRA_TITLE);
        if(Objects.equals(title,currentTitle)) {
            logger.warn("*** Title {} is already set, no need to update", currentTitle);
        } else {
            applicationProperties.setString(APKeys.JIRA_TITLE, title);
            logger.warn("*** Updated title from {} to {}", currentTitle, title);
        }
    }

    @Nonnull
    @Override
    public String getAdminGroup() {
        return "jira-administrators";
    }
}