This Plugin performs several inizialization tasks after being enabled

It checks the PluginSetting **de.resolution.applicationinit.state** for the string-value **already_initialized**
and stops if it is set. If not, it sets that value to avoid repeated execution before running.
 
To reset this on the database, e.g. after restoring a backup containing this value, use this query:

#### Jira:
    update propertystring set propertyvalue = 'doitagain'  where id in (select ps.id from propertyentry pe join propertystring ps on ps.id = pe.id where property_key = 'de.resolution.applicationinit.state');
#### Confluence
    update bandana set bandanavalue = 'doitagain' where bandanakey = 'de.resolution.applicationinit.state';
#### Bitbucket
    update plugin_setting set key_value = 'doitagain' where key_name = 'de.resolution.applicationinit.state';
#### Bamboo
    update bandana set serialized_data = '<string>doitagain</string>' where bandana_key = 'de.resolution.applicationinit.state';
Bandana values are cached, so re-enabling ApplicationInit in the UPM will not re-run it without restarting Confluence
    

## Environment variables
The following environment-variables are used:

* ALWAYS_RUN_APPLICATION_INIT: Set this to true to run even if the PluginSetting is set
* ATLASSIAN_BASEURL: Baseurl to be set
* ATLASSIAN_LICENSE: License to set
* ATLASSIAN_TITLE: Title to set
* ATLASSIAN_ADMIN_USER: Userid of a administrator-user to be created or updated
* ATLASSIAN_ADMIN_PASSWORD: Password for the admin-user to be created or updated. Mandatory if ATLASSIAN_ADMIN_USER is set
* ATLASSIAN_ADMIN_EMAIL: Email for the admin user, only used when creating a new user, defaults to admin@example.com
* ATLASSIAN_ADMIN_DISPLAYNAME: Display-Name for the admin user, only used when creating a new user, defaults to Admin User

## What is done
If the appropriate environment-variables are set, the following actions are performed:

*  Set the Base-URL
*  Install a license
*  Set the application's title
*  Create or update a user with administrator-privileges. Username and password must be set.
