package de.resolution.applicationinit;

import com.atlassian.crowd.embedded.api.*;
import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.exception.InvalidUserException;
import com.atlassian.crowd.exception.OperationNotPermittedException;
import com.atlassian.crowd.model.user.UserTemplateWithAttributes;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import de.resolution.commons.license.LicenseInstaller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;

public abstract class ApplicationInit  {

    private static final Logger logger = LoggerFactory.getLogger(ApplicationInit.class);

    private final CrowdDirectoryService crowdDirectoryService;
    private final CrowdService crowdService;
    private final PluginSettingsFactory pluginSettingsFactory;

    private final LicenseInstaller licenseInstaller;

    private static final String ENV_BASEURL           = "ATLASSIAN_BASEURL";
    private static final String ENV_LICENSE           = "ATLASSIAN_LICENSE";
    private static final String ENV_TITLE             = "ATLASSIAN_TITLE";
    private static final String ENV_ADMIN_USER        = "ATLASSIAN_ADMIN_USER";
    private static final String ENV_ADMIN_PASSWORD    = "ATLASSIAN_ADMIN_PASSWORD";  //NOSONAR this is on purpose
    private static final String ENV_ADMIN_EMAIL       = "ATLASSIAN_ADMIN_EMAIL";
    private static final String ENV_ADMIN_DISPLAYNAME = "ATLASSIAN_ADMIN_DISPLAYNAME";

    private static final String ENV_RUN_ALWAYS = "ALWAYS_RUN_APPLICATION_INIT";

    private static final String DEFAULT_ADMIN_DISPLAYNAME = "Admin User";
    private static final String DEFAULT_ADMIN_EMAIL       = "admin@example.com";

    private static final String SETTINGS_KEY = "de.resolution.applicationinit.state";
    private static final String SETTINGS_VALUE_IF_RUN = "already_initialized";

    private static final String ENV_LICENSE_PREFIX ="LICENSE_";

    private static final String ENV_PLUGIN_LICENSE_URL = "PLUGIN_LICENSE_URL";

    ApplicationInit(PluginSettingsFactory pluginSettingsFactory, CrowdService crowdService, CrowdDirectoryService crowdDirectoryService) {
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.crowdDirectoryService = crowdDirectoryService;
        this.crowdService = crowdService;
        this.licenseInstaller = new LicenseInstaller(pluginSettingsFactory);
    }

    void installPluginLicenses() {

        logger.warn("*** Pre-installing Plugin Licenses from ENV ***");
        System.getenv().keySet().stream()
                .filter(key -> key.startsWith(ENV_LICENSE_PREFIX)).forEach(envKey -> {
                    String appKey = envKey.substring(ENV_LICENSE_PREFIX.length());
                    String licenseString = System.getenv(envKey);
                    logger.warn("*** Installing: {} : {} ", appKey, licenseString);
                    licenseInstaller.storeLicense(appKey, licenseString);
        });

        String pluginLicenseUrl = System.getenv(ENV_PLUGIN_LICENSE_URL);
        if(pluginLicenseUrl != null && !pluginLicenseUrl.trim().isEmpty()) {
            logger.warn("*** Fetching licenses from {}", pluginLicenseUrl);
            Map<String, String> licenses = fetchLicenses(pluginLicenseUrl);
            licenses.forEach((key, lic) -> {
                logger.warn("*** Installing: {} : {} ", key, lic);
                licenseInstaller.storeLicense(key, lic);
            });
        }
        logger.warn("*** Plugin License install done ***");
    }

    void run() {

       PluginSettings pluginSettings = pluginSettingsFactory.createGlobalSettings();
       String settingsValue = (String)pluginSettings.get(SETTINGS_KEY);
       if(Objects.equals(SETTINGS_VALUE_IF_RUN,settingsValue)
               && (System.getenv(ENV_RUN_ALWAYS) == null
                || !Boolean.parseBoolean(System.getenv(ENV_RUN_ALWAYS)))) {
           logger.warn("******** Found settings key {} with value {}, skipping ApplicationInit ********", SETTINGS_KEY, SETTINGS_VALUE_IF_RUN);
           return;
       }

        logger.warn("************* Running ApplicationInit ***********");

        String newBaseUrl = System.getenv(ENV_BASEURL);
        if(notEmpty(newBaseUrl)) {
            setBaseUrl(newBaseUrl);
        } else {
            logger.warn("*** {} is not set, not updating the base url", ENV_BASEURL);
        }

        String license = System.getenv(ENV_LICENSE);
        if(notEmpty(license)) {
            logger.warn("*** Updating license ");
            setLicense(license);
        } else {
            logger.warn("*** {} is not set, not updating the license", ENV_LICENSE);
        }

        String title = System.getenv(ENV_TITLE);
        if(notEmpty(title)) {
            setTitle(title);
        } else {
            logger.warn("*** {} is not set, not updating the title", ENV_TITLE);
        }

        String adminUser = System.getenv(ENV_ADMIN_USER);
        if(notEmpty(adminUser)) {

            String adminPassword = System.getenv(ENV_ADMIN_PASSWORD);
            if(adminPassword != null && !adminPassword.trim().isEmpty()) {
                String adminEmail = readEnv(ENV_ADMIN_EMAIL, DEFAULT_ADMIN_EMAIL);
                String adminDisplayName = readEnv(ENV_ADMIN_DISPLAYNAME, DEFAULT_ADMIN_DISPLAYNAME);
                createOrUpdateAdminUser(adminUser, adminDisplayName,adminEmail,adminPassword);
            } else {
                logger.warn("*** {} is not set, not creating or updating user {}", ENV_ADMIN_PASSWORD, adminUser);
            }
        } else {
            logger.warn("*** {} is not set, not creating or updating the admin user", ENV_ADMIN_USER);
        }

        installPluginLicenses();


        pluginSettings.put(SETTINGS_KEY,SETTINGS_VALUE_IF_RUN);

        logger.warn("************** ApplicationInit done *************");
    }

    private boolean notEmpty(@Nullable String str) {
        return str != null && !str.trim().isEmpty();
    }

    public abstract void setBaseUrl(@Nonnull String baseUrl);

    public abstract void setLicense(@Nonnull String license);

    public abstract void setTitle(@Nonnull String title);

    @Nonnull
    public abstract String getAdminGroup();

    private long getInternalDirectory() {
        return crowdDirectoryService.findAllDirectories().stream()
                .filter(dir -> dir.getType() == DirectoryType.INTERNAL)
                .min(Comparator.comparing(Directory::getId))
                .orElseThrow(() -> new RuntimeException("No internal directory found"))
                .getId();
    }

    public void createOrUpdateAdminUser(@Nonnull String username, @Nonnull String displayName, @Nonnull String email, @Nonnull String password) {

        User adminUser = crowdService.getUser(username);
        if(adminUser != null) {
            logger.warn("User {} is present, just resetting the password",username);
            try {
                crowdService.updateUserCredential(adminUser,password);
            } catch (InvalidCredentialException | OperationNotPermittedException e) {
                logger.error("Resetting password for user {} failed", username, e);
            }
        } else {
            logger.warn("Creating new admin user {}", username);

            UserTemplateWithAttributes userTemplate =
                    new UserTemplateWithAttributes(username,getInternalDirectory());
            userTemplate.setActive(true);
            userTemplate.setEmailAddress(email);
            userTemplate.setDisplayName(displayName);

            try {
                User createdUser = crowdService.addUser(userTemplate,password);
                Group adminGroup = crowdService.getGroup(getAdminGroup());
                if(adminGroup == null) {
                    logger.error("Could not load admin group {}",getAdminGroup());
                }
                if(!crowdService.addUserToGroup(createdUser,adminGroup)) {
                    logger.error("Could not add user {} to group {}", username, getAdminGroup());
                }
            } catch (InvalidUserException | InvalidCredentialException | OperationNotPermittedException e) {
                logger.error("Creating admin user failed",e);
            }
        }
    }

    private String readEnv(String key, String defaultValue) {
        String fromEnv = System.getenv(key);
        if(fromEnv == null || fromEnv.trim().isEmpty()) {
            return defaultValue;
        } else {
            return fromEnv;
        }
    }

    public static Map<String,String> fetchLicenses(String url) {

        logger.info("Fetching license from {}", url);

        if(url == null || url.trim().isEmpty()) {
            return Collections.emptyMap();
        }

        Map<String,String> licenses = new HashMap<>();

        try {
            HttpURLConnection http = (HttpURLConnection) new URL(url).openConnection();
            http.setRequestMethod("GET");

            InputStream inputStream = http.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while((line = br.readLine()) != null) {
                String[] splitted = line.split(":");
                if (splitted.length > 1) {
                    licenses.put(splitted[0],splitted[1]);
                }
            }
            br.close();
            inputStream.close();

        } catch (IOException e) {
            logger.warn("*** Fetching licenses failed: {}", e.getMessage());
            return Collections.emptyMap();
        }
        return licenses;
    }
}
