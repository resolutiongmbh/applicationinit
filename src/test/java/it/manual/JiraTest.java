package it.manual;

import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import de.resolution.applicationinit.JiraApplicationInit;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;

@SuppressWarnings({"SpringJavaAutowiredMembersInspection", "unused"})
@RunWith(Arquillian.class)
public class JiraTest {

    @ComponentImport @Autowired
    private JiraApplicationInit jiraApplicationInit;

    @Test
    public void setBaseUrl() { //NOSONAR this is for manual testing only
        jiraApplicationInit.setBaseUrl("https://dev-sd3160-postgres.lab.resolution.de/");
    }

    @Test
    public void setLicense() { //NOSONAR this is for manual testing only
        String swLicense = "ADD HERE FOR TEST";
        String sdLicense = "ADD HERE FOR TEST";
    }

    @Test
    public void setTitle() { //NOSONAR this is for manual testing only
        jiraApplicationInit.setTitle("Jira Test instance");
    }

    @Test
    public void setAdminUser() { //NOSONAR this is for manual testing only
        jiraApplicationInit.createOrUpdateAdminUser("adminTest","Admin Test User", "admin@test.com", "blafasel");
    }



    @Deployment
    public static Archive<?> deployTests() {
        return ShrinkWrap.create(JavaArchive.class, "tests.jar");
    }
}