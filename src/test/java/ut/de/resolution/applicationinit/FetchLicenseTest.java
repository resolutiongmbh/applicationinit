package ut.de.resolution.applicationinit;

import de.resolution.applicationinit.ApplicationInit;
import org.junit.Assert;
import org.junit.Test;

import java.util.Map;

public class FetchLicenseTest {

    @Test
    public void fetchWithNullOrEmptyOrInvalidUrl() {
        Assert.assertTrue(ApplicationInit.fetchLicenses("").isEmpty());
        Assert.assertTrue(ApplicationInit.fetchLicenses(" ").isEmpty());
        Assert.assertTrue(ApplicationInit.fetchLicenses(null).isEmpty());
        Assert.assertTrue(ApplicationInit.fetchLicenses("abdee").isEmpty());
        Assert.assertTrue(ApplicationInit.fetchLicenses("https://s3.amazonaws.com/public.resolution.de/not_there").isEmpty());
    }

    @Test
    public void testFetch() {
        String url = "https://s3.eu-central-1.amazonaws.com/provisioning.reslab.de/lic_test.txt";
        Map<String,String> licenses = ApplicationInit.fetchLicenses(url);
        Assert.assertEquals(4,licenses.size());
    }
}
