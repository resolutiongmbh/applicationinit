package it.manual;

import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import de.resolution.applicationinit.ConfluenceApplicationInit;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;

@RunWith(Arquillian.class)
public class ConfluenceTest {

    @ComponentImport @Autowired
    private ConfluenceApplicationInit confluenceApplicationInit;

    @Test
    public void setBaseUrl() { //NOSONAR this is for manual testing only
        confluenceApplicationInit.setBaseUrl("https://it-conf-6130-postgres.lab.resolution.de");
    }

    @Test
    public void setLicense() { //NOSONAR this is for manual testing only
        String devLicense ="INSERT LICENSE HERE TO TEST";
        confluenceApplicationInit.setLicense(devLicense);
    }

    @Test
    public void setTitle() { //NOSONAR this is for manual testing only
        confluenceApplicationInit.setTitle("Title Test");
    }

    @Deployment
    public static Archive<?> deployTests() {
        return ShrinkWrap.create(JavaArchive.class, "tests.jar");
    }
}