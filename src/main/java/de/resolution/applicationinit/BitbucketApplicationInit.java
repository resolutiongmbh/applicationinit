package de.resolution.applicationinit;

import com.atlassian.bitbucket.license.LicenseService;
import com.atlassian.bitbucket.permission.Permission;
import com.atlassian.bitbucket.permission.PermissionAdminService;
import com.atlassian.bitbucket.permission.SetPermissionRequest;
import com.atlassian.bitbucket.server.ApplicationPropertiesService;
import com.atlassian.bitbucket.user.SecurityService;
import com.atlassian.bitbucket.util.UncheckedOperation;
import com.atlassian.crowd.embedded.api.CrowdDirectoryService;
import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.exception.OperationNotPermittedException;
import com.atlassian.crowd.exception.embedded.InvalidGroupException;
import com.atlassian.plugin.spring.scanner.annotation.component.BitbucketComponent;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.BitbucketImport;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Nonnull;
import java.net.URI;
import java.util.Objects;

@BitbucketComponent
@ExportAsService
public class BitbucketApplicationInit extends ApplicationInit {

    private static final Logger logger = LoggerFactory.getLogger(BitbucketApplicationInit.class);

    private final ApplicationPropertiesService applicationPropertiesService;
    private final LicenseService licenseService;
    private final PermissionAdminService permissionAdminService;
    private final CrowdService crowdService;

    @Autowired
    public BitbucketApplicationInit(
            @BitbucketImport PluginSettingsFactory pluginSettingsFactory,
            @BitbucketImport CrowdService crowdService,
            @BitbucketImport CrowdDirectoryService crowdDirectoryService,
            @BitbucketImport ApplicationPropertiesService applicationPropertiesService,
            @BitbucketImport LicenseService licenseService,
            @BitbucketImport PermissionAdminService permissionAdminService,
            @BitbucketImport SecurityService securityService) {
        super(pluginSettingsFactory, crowdService, crowdDirectoryService);
        this.crowdService = crowdService;
        this.licenseService = licenseService;
        this.applicationPropertiesService = applicationPropertiesService;
        this.permissionAdminService = permissionAdminService;

        securityService.withPermission(Permission.SYS_ADMIN, "create new admin user").call((UncheckedOperation<Void>) () -> {
            run();
            return null;
        });
    }

    @Override
    public void setBaseUrl(@Nonnull String baseUrl) {
        URI wantedBaseUrl = URI.create(baseUrl);
        URI currentBaseUrl = applicationPropertiesService.getBaseUrl();
        if (Objects.equals(wantedBaseUrl, currentBaseUrl)) {
            logger.warn("*** Base url {} is already set, no need to update", currentBaseUrl);
        } else {
            applicationPropertiesService.setBaseURL(wantedBaseUrl);
            logger.warn("*** Updated base url from {} to {}", currentBaseUrl, baseUrl);
        }
    }

    @Override
    public void setLicense(@Nonnull String license) {
        try {
            String currentLicense = licenseService.getAsString();

            if (Objects.equals(currentLicense, license)) {
                logger.warn("*** This license is already installed.");
            } else {
                licenseService.set(license);
                logger.warn("*** license updated");
            }
        } catch (Exception e) {
            logger.error("Setting license failed", e);
        }
    }

    @Override
    public void setTitle(@Nonnull String title) {
        String currentTitle = applicationPropertiesService.getDisplayName();
        if (Objects.equals(title, currentTitle)) {
            logger.warn("*** Title {} is already set, no need to update", currentTitle);
        } else {
            applicationPropertiesService.setDisplayName(title);
            logger.warn("*** Updated title from {} to {}", currentTitle, title);
        }
    }

    @Nonnull
    @Override
    public String getAdminGroup() {
        String adminGroupName = "stash-administrators";
        Group adminGroup = crowdService.getGroup(adminGroupName);
        if (adminGroup == null) {
            logger.warn("*** Admin Group {} doesn't exist yet. Creating...", adminGroupName);

            // Create group
            try {
                crowdService.addGroup(new Group() {
                    @Override
                    public String getName() {
                        return adminGroupName;
                    }

                    @Override
                    public int compareTo(Group group) {
                        return getName().compareTo(group.getName());
                    }
                });
            } catch (InvalidGroupException | OperationNotPermittedException e) {
                logger.error("*** Got an exception while creating admin group ***", e);
                throw new IllegalStateException(e);
            }
            logger.warn("*** Admin Group {} created! Assigning permissions...", adminGroupName);
            try {
                permissionAdminService.setPermission(new SetPermissionRequest.Builder().group(adminGroupName).globalPermission(Permission.SYS_ADMIN).build());
            } catch (Exception e) {
                logger.error("*** Got an exception while granting admin group permissions ***", e);
                throw new IllegalStateException(e);
            }
            logger.warn("*** Admin Group {} was granted SYS_ADMIN permission!", adminGroupName);

        }
        return adminGroupName;
    }
}
