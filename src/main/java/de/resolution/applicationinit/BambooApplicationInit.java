package de.resolution.applicationinit;

import com.atlassian.bamboo.configuration.AdministrationConfiguration;
import com.atlassian.bamboo.configuration.AdministrationConfigurationAccessor;
import com.atlassian.bamboo.configuration.AdministrationConfigurationPersister;
import com.atlassian.bamboo.license.BambooLicenseManager;
import com.atlassian.crowd.embedded.api.CrowdDirectoryService;
import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.spring.scanner.annotation.component.BambooComponent;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.BambooImport;
import com.atlassian.sal.api.license.LicenseHandler;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nonnull;
import java.util.Objects;

@BambooComponent
@ExportAsService
public class BambooApplicationInit extends ApplicationInit {

    private static final Logger logger = LoggerFactory.getLogger(BambooApplicationInit.class);

    private final AdministrationConfigurationAccessor administrationConfigurationAccessor;
    private final AdministrationConfigurationPersister administrationConfigurationPersister;
    private final BambooLicenseManager bambooLicenseManager;
    private final LicenseHandler licenseHandler;
    private final TransactionTemplate transactionTemplate;

    @Autowired
    public BambooApplicationInit(
            @BambooImport AdministrationConfigurationAccessor administrationConfigurationAccessor,
            @BambooImport AdministrationConfigurationPersister administrationConfigurationPersister,
            @BambooImport BambooLicenseManager bambooLicenseManager,
            @BambooImport LicenseHandler licenseHandler,
            @BambooImport CrowdService crowdService,
            @BambooImport TransactionTemplate transactionTemplate,
            @BambooImport EventPublisher eventPublisher,
            @BambooImport PluginSettingsFactory pluginSettingsFactory,
            @BambooImport CrowdDirectoryService crowdDirectoryService) {
        super(pluginSettingsFactory, crowdService, crowdDirectoryService);

        this.administrationConfigurationAccessor = administrationConfigurationAccessor;
        this.administrationConfigurationPersister = administrationConfigurationPersister;
        this.bambooLicenseManager = bambooLicenseManager;
        this.licenseHandler = licenseHandler;
        this.transactionTemplate = transactionTemplate;

        try {
            runInTransaction();
            return;
        } catch (RuntimeException e) {
            logger.warn("Encountered an exception while trying to initialize Bamboo. This is normal on Bamboo while we're waiting for the database to become ready. Waiting for ServerStartedEvent to try again...");
            logger.debug("Exception: ", e);
        }
        eventPublisher.register(this);
    }

    @EventListener
    public void onEvent(Object event) {
        if (event.toString().contains("com.atlassian.bamboo.event.ServerStartedEvent")) {
            System.out.println("Handled an event: " + event);
            transactionTemplate.execute(() -> {
                this.run();
                return null;
            });
        }
    }

    private void runInTransaction() {
        transactionTemplate.execute(() -> {
            this.run();
            return null;
        });
    }

    @Override
    public void setBaseUrl(@Nonnull String baseUrl) {

        AdministrationConfiguration administrationConfiguration = administrationConfigurationAccessor.getAdministrationConfiguration();
        String currentBaseUrl = administrationConfiguration.getBaseUrl();
        if (Objects.equals(baseUrl, currentBaseUrl)) {
            logger.warn("*** Base url {} is already set, no need to update", currentBaseUrl);
        } else {
            administrationConfiguration.setBaseUrl(baseUrl);
            administrationConfigurationPersister.saveAdministrationConfiguration(administrationConfiguration);
            logger.warn("*** Updated base url from {} to {}", currentBaseUrl, baseUrl);
        }
    }

    @Override
    public void setLicense(@Nonnull String license) {
        try {
            String rawProductLicense = licenseHandler.getRawProductLicense("bamboo");
            if (Objects.equals(rawProductLicense, license)) {
                logger.warn("*** This license is already installed.");
            } else {
                bambooLicenseManager.setLicense(license);
                logger.warn("*** license updated");
            }
        } catch (Exception e) {
            logger.error("Setting license failed", e);
        }
    }

    @Override
    public void setTitle(@Nonnull String title) {
        AdministrationConfiguration administrationConfiguration = administrationConfigurationAccessor.getAdministrationConfiguration();
        String currentTitle = administrationConfiguration.getInstanceName();
        if (Objects.equals(title, currentTitle)) {
            logger.warn("*** Title {} is already set, no need to update", currentTitle);
        } else {
            administrationConfiguration.setInstanceName(title);
            administrationConfigurationPersister.saveAdministrationConfiguration(administrationConfiguration);
            logger.warn("*** Updated title from {} to {}", currentTitle, title);
        }
    }

    @Nonnull
    @Override
    public String getAdminGroup() {
        return "bamboo-admin";
    }
}
