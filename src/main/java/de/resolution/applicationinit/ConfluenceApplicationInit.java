package de.resolution.applicationinit;

import com.atlassian.confluence.event.events.admin.GlobalSettingsChangedEvent;
import com.atlassian.confluence.license.LicenseService;
import com.atlassian.confluence.setup.settings.Settings;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.crowd.embedded.api.CrowdDirectoryService;
import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.extras.api.confluence.ConfluenceLicense;
import com.atlassian.plugin.spring.scanner.annotation.component.ConfluenceComponent;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ConfluenceImport;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Nonnull;
import java.util.Objects;

@ConfluenceComponent
@ExportAsService
public class ConfluenceApplicationInit extends ApplicationInit {

    private static final Logger logger = LoggerFactory.getLogger(ConfluenceApplicationInit.class);

    private final SettingsManager settingsManager;
    private final LicenseService licenseService;
    private final EventPublisher eventPublisher;

    @Autowired
    public ConfluenceApplicationInit(
            @ConfluenceImport PluginSettingsFactory pluginSettingsFactory,
            @ConfluenceImport CrowdService crowdService,
            @ConfluenceImport CrowdDirectoryService crowdDirectoryService,
            @ConfluenceImport SettingsManager settingsManager,
            @ConfluenceImport LicenseService licenseService,
            @ConfluenceImport EventPublisher eventPublisher) {
        super(pluginSettingsFactory,crowdService,crowdDirectoryService);
        this.settingsManager = settingsManager;
        this.licenseService  = licenseService;
        this.eventPublisher  = eventPublisher;
        run();
    }

    @Override
    public void setBaseUrl(@Nonnull String baseUrl) {

        Settings settings = settingsManager.getGlobalSettings();
        String currentBaseUrl = settings.getBaseUrl();
        if(Objects.equals(baseUrl,currentBaseUrl)) {
            logger.warn("*** Base url {} is already set, no need to update", currentBaseUrl);
        } else {
            Settings newSettings = new Settings(settings);
            newSettings.setBaseUrl(baseUrl);
            settingsManager.updateGlobalSettings(newSettings);
            GlobalSettingsChangedEvent event = new GlobalSettingsChangedEvent(this,settings,newSettings);
            eventPublisher.publish(event);
            logger.warn("*** Updated base url from {} to {}", currentBaseUrl, baseUrl);
        }
    }

    @Override
    public void setLicense(@Nonnull String license) {
        try {
            ConfluenceLicense currentLicense = licenseService.retrieve();
            ConfluenceLicense decodedLicense = licenseService.validate(license);

            if(Objects.equals(currentLicense,decodedLicense)) {
                logger.warn("*** This license is already installed.");
            } else {
                licenseService.install(license);
                logger.warn ("*** license updated");
            }
        } catch (Exception e) {
            logger.error("Setting license failed",e);
        }
    }

    @Override
    public void setTitle(@Nonnull String title) {
        Settings settings = settingsManager.getGlobalSettings();
        String currentTitle = settings.getSiteTitle();
        if(Objects.equals(title,currentTitle)) {
            logger.warn("*** Title {} is already set, no need to update", currentTitle);
        } else {
            settings.setSiteTitle(title);
            settingsManager.updateGlobalSettings(settings);
            logger.warn("*** Updated title from {} to {}", currentTitle, title);
        }
    }

    @Nonnull
    @Override
    public String getAdminGroup() {
        return "confluence-administrators";
    }
}
